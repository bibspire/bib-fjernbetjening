const video = document.querySelector("video");
const canvas = document.querySelector("canvas");
canvas.width = 40;
canvas.height = 30;
let prevData;

let prevTime;
let deltas = [];
let deltaPos = 0;
const maxDelta = 10;
let prevprevMove = false;
let prevMove = false;
async function draw() {
  // make sure we have a new frame
  if (video.currentTime == prevTime) return;
  prevTime = video.currentTime;

  let ctx = canvas.getContext("2d");
  ctx.drawImage(video, 0, 0, canvas.width, canvas.height);
  let data = ctx.getImageData(0, 0, canvas.width, canvas.height).data;
  if (prevData) {
    let delta = 0;
    for (let i = 0; i < data.length; ++i) {
      let d = data[i] - prevData[i];
      delta += d * d;
      //diff += Math.abs(data[i] - prevData[i]);
    }
    delta /= data.length;
    //console.log(diff);
    deltas[deltaPos] = delta;
    deltaPos = (deltaPos + 1) % maxDelta;

    let t = deltas.slice(0);
    t.sort();
    move = delta > t[(maxDelta * 0.5) | 0] * 1.3;
    document.querySelector(
      "#status"
    ).innerHTML = move /*&& prevMove && prevprevMove*/
      ? "<h1>Movement detected</h1>"
      : JSON.stringify({ delta, x: t[(maxDelta / 5) | 0] });
    prevprevMove = prevMove;
    prevMove = move;
  }
  prevData = data.slice(0);
  //console.timeEnd('process')
}

async function main() {
  let stream = await navigator.mediaDevices.getUserMedia({
    audio: false,
    video: true,
  });
  video.srcObject = stream;
  video.play();
  //video.ontimeupdate = draw;
  setInterval(draw, 200);
}
main();
console.log("hello world");
