(() => {
  //////////////////////////////////////////////
  //
  // Konfiguration. Her er webadressen for
  // ansigtsgenkendelses-webapp'en. Ændr denne,
  // hvis den hostes et andet sted:
  //
  const url = "https://demo.solsort.dk/bib-fjernbetjening/";

  // Navn på brugeren i whereby:
  const username = "username";

  //////////////////////////////////////////////
  //
  // Selve koden herunder, der navigere på Whereby-sitet er herunder.
  //
  // Bemærk, at hvis Whereby ændre teksten på deres hjemmeside,
  // så vil scriptet sandsynligvis ophøre med at virke.
  // Dette I selv løse ved at rette i teksten i main-funktionen,
  // eller jeg kan rette det for jer.
  //
  // Kernen i scriptet er main-funktionen,
  // der udføreres hver ½ sekund når man er inde på whereby.com.
  //

  // Hjælpefunktioner:
  const clickButton = (text) =>
    Array.from(document.querySelectorAll("button"))
      .find((b) => b.innerText === text)
      .click();
  const redirect = () => {
    location.href = `${url}?url=${encodeURIComponent(location.href)}`;
  };

  //
  // Main-funktionen.
  //
  // Hvis scriptet holder op med at virke, på grund af ændringer på whereby.com
  // så kan det muligvis løses ved at rette teksterne i denne funktion.
  //
  // Koden herunder tjekker om hjemmesiden forøjeblikket indeholder
  // de givne tekster, og i så fald klikker den enten på knap eller redirecter.
  //
  async function main() {
    let s = document.body.innerHTML;

    if (s.match("You’re about to join a video meeting")) {
      if (
        s.match("Your name") &&
        s.match("Enter your name") &&
        s.match("Have an account?") &&
        s.match("Continue")
      ) {
        let name = document.querySelector("input");
        if (name) {
          name.value = username;
        }
        clickButton("Continue");
        return;
      }

      if (
        s.match(
          "For others to see and hear you, your browser will request access to your cam and mic."
        ) &&
        s.match("Request permissions")
      ) {
        clickButton("Request permissions");
        return;
      }
      if (
        s.match("This room is locked") &&
        s.match("The host will see your picture and name when you knock") &&
        s.match("and will let you in when they are ready.") &&
        s.match("Knock")
      ) {
        clickButton("Knock");
      }

      if (
        s.match("You’ve not been granted access") &&
        s.match(
          "If you were invited to this room, please contact the person who invited you"
        ) &&
        s.match("Leave room")
      ) {
        redirect();
      }
    }
    if (
      s.match("ended the meeting") &&
      s.match("That’s a wrap. Want to host your own flexible meetings?")
    ) {
      redirect();
    }
    if (
      s.match("You’ve left the room") &&
      s.match("That’s a wrap. Want to host your own flexible meetings?")
    ) {
      redirect();
    }
  }
  setInterval(main, 500);
})();
