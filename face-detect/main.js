////////
//
// Konfiguration som I let selv kan tilpasse:
//

// Hvor stor en del af skærmen ansigtet skal fylde, før end at den redirecter.
const threshold = 0.2;

// Hvor mange sekunder den skal se et ansigt, før end at den redirecter til whereby:
const secondsToWait = 5;

// Teksten som den viser på skærmen:
const statusMessageNoFace = (url) => `

  Flyt dit ansigt tættere på kameraet for at kontakte en bibliotekar.

  <br><br><div style="font-size: 32px">

  (eller klik <a href="${url}">her</a>)

  </div>
  `;

const statusMessageFaceSeen = (seconds, url) => `

  Kontakter bibliotekar om ${secondsToWait - seconds} sekunder.

  <br><br>

  Gå væk fra kameraet for at fortryde.
  `;

// Hvilket whereby-rum der skal redirectes til, hvis man ikke giver det med som parameter:
const defaultUrl = "https://whereby.com/wbtest";

///////////////////////////////////
//
// Selve koden er herunder.
//

let url =
  location.search && location.search.startsWith("?url=")
    ? decodeURIComponent(location.search.replace("?url=", ""))
    : defaultUrl;
const video = document.querySelector("video");
const canvas = document.querySelector("canvas");
let w, h, ctx;

async function layout() {
  w = video.clientWidth;
  h = video.clientHeight;
  canvas.width = w;
  canvas.height = h;

  document.body.style.overflow = "hidden";
  let elem = document.querySelector("#container");
  let scale = Math.max(window.innerWidth / w, window.innerHeight / h);
  elem.style.transform = `scale(${scale})`;
  elem.style.left = `${(window.innerWidth - w * scale) / 2}px`;
  elem.style.top = `${(window.innerHeight - h * scale) / 2}px`;

  ctx = canvas.getContext("2d");
  ctx.fillStyle = "rgba(200,200,200,0.8)";
  ctx.fillRect(0, 0, w, h);
}

let detectCount = 0;
async function process() {
  let detections = await faceapi.detectAllFaces(video);
  layout();

  let detected = false;
  if (detections[0]) {
    let { _x, _y, _width, _height } = detections[0]._box;
    let x = _x + _width / 2;
    let y = _y + _height / 2;
    let size = Math.sqrt(_width * _height);
    if (size > Math.sqrt(w * h) * threshold) {
      detected = true;
    }
    ctx.clearRect(_x, _y, _width, _height);
    ctx.fillStyle = "rgba(255,255,255,0.5)";
    ctx.fillRect(_x, _y, _width, _height);
  }
  if (detected) {
    ++detectCount;
  } else {
    detectCount = 0;
  }
  if (detectCount >= secondsToWait) {
    location.href = url;
  }

  let status = document.querySelector("#status");
  if (detectCount == 0) {
    status.innerHTML = statusMessageNoFace(url);
  } else {
    status.innerHTML = statusMessageFaceSeen(detectCount, url);
  }
}
let sleep = (ms) => new Promise((resolve) => setTimeout(resolve, ms));

async function main() {
  let stream = await navigator.mediaDevices.getUserMedia({
    audio: false,
    video: true,
  });
  video.srcObject = stream;
  video.play();
  await sleep(100);
  layout();

  await faceapi.loadSsdMobilenetv1Model("models");
  setInterval(process, 1000);
}
main();
