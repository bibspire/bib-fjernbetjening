# Biblioteks-fjernbetjening

Værktøj til biblioteksbetjening over videokonference. Berøringsfrit for at undgå Corona-smitte.  Projektet indeholder følgende:

- `autoknock-ext` Chrome-extension til automatisk at banke på / logge ind i rum i Whereby, – samt går tilbage til webapp'en når konversationen er slut.
- `face-detect` webapp, der via kamera ser, om der er en bruger som gerne vil tale med en bibliotekar, og i så fald sender brugeren over i et givent rum i Whereby
- (`movement-detect` – simpel algoritme til at detektere bevægelse, – oprindelige koncept til webapp'en, men erstattet af `face-detect`, bruges ikke længere)
- `README.md` – dette dokument. Vejledning til brug af løsningen.

## Chrome-extension til automatisk forbindelse til Whereby-rum

`autoknock-ext` er en udvidelse af chrome, der gør at broweren automatisk navigerer på whereby.com således:

- Når man går ind til et rum, så udfylder browseren automatisk brugernavn, og klikker på "Knock"-knappen.
- Når man er færdig med samtalen, redirectes browseren hen til ansigtsgenkendelses-webapp'en, og medsender info om hvilket Whereby-rum ansigtgenkendelsen skal sende brugeren til. Foreløbigt ligger webapp'en på <https://demo.solsort.dk/bib-fjernbetjening/>, men denne kan let flyttes over på bibliotekets egen server.



### Installation

Chrome-udvidelsen skal installeres i browseren som en udvikler-extension – hvilket består af flere trin:

1. Hent kildekoden til browser-extensionen herfra: <https://demo.solsort.dk/bib-fjernbetjening/autoknock-ext.zip> (alternativt kan den også hentes direkte fra gitlab.com/bibspire/bib-fjernbetjening)
2. Pak zip-filen ud, – dette opretter en mappe `autoknock-ext`.
3. Tilføj udvidelsen til Chrome som "udvikler". Start Chrome og gør følgende:
    1. Skriv `chrome://extensions` i adresse-linjen og tryk enter.
    2. Slå "Developer mode" til (klik på toggle-knappen øverst til højre på siden).
    3. Klik på "Load unpacked".
    4. Vælg mappen `autoknock-ext`.
    5. (Klik på genindlæsnings-ikonet i bunden af "balbib-wb-autoknock", hvis I ændre konfigurationen/koden)
4. Gå ind på det rum i Whereby, som I ønsker at computeren skal forbinde til. Herefter herefter burde den automatisk logge ind.

Nogle udgaver af Chrome giver advarsel ved opstart og foreslår at fjerne udvidelsen, da den ikke er en del af Googles "Chrome Web Store". Dette kan man undgå ved at benytte udvikler-udgaven af Chrome.


### Konfiguration

Chrome-udvidelsen kan tilpasses, med brugernavn for den pågældende computer, samt hvilken adresse ansigtsgenkendelses-webapp'en ligger på. Dette kan gøres således:

1. Åbn mappen `autoknock-ext`.
2. Højreklik på `content-script.js` og vælg "Open with..." i menu'en (Sandsynligvis "Åbn med..." på dansk)
3. Vælg Notepad (sandsynligvis Notesblok), herved åbnes koden som et tekstdokument.
4. Øverst i dokumentet kan man ændre url'en og brugernavn. Dette er også beskrevet i kommentarerne i selve dokumentet.
5. Gem dokumentet (i menu'en eller med Ctrl+S).
6. Genindlæs Chrome-udvidelsen for at den nye konfiguration benyttes. Dette er beskrevet ovenover i afsnittet "Installation".

Hvis der går noget galt med konfigurationen, så kan man altid geninstallere plugin'et, – hvorefter det hele virker igen. 

### Vigtigt omkring Whereby

**Det er bibliotekets eget ansvar, at undersøge / sikre om I skal have et abonnement eller aftale med Whereby, for at have rettigheder til at bruge deres site på denne måde.** Jeg har blot bygget scriptet udfra bibliotekets ønsker, og har ikke sat mig ind i rettighederne for hvordan Whereby må benyttes.

**Hvis Whereby ændrer indholdet i sin grænseflade, så er der risiko for at denne Chrome-udvidelse holder op med at fungere.** I dokumentationen af koden i `autoknock-ext/content-script.js` er der beskrevet hvordan man muligvis selv kan rette dette, – og ellers kigger jeg også gerne på det.

## Automatisk opkald til Whereby ved ansigtsgenkendelse

`face-detect` er en webapp, der redirecter til et en anden adresse (eksempelvis et Whereby-rum), når der er et ansigt er tilstrækkeligt tæt på kameraet.

Du kan prøve den ved at gå ind på [https://demo.solsort.dk/bib-fjernbetjening/?url=https://whereby.com/wbtest](https://demo.solsort.dk/bib-fjernbetjening/?url=https%3A%2F%2Fwhereby.com%2Fwbtest). Hvis du ønsker at redirecte til et andet Whereby-rum (eller site) kan du ændre `url=...` i slutningen af adressen. Chrome-udvidelsen `autoknock-ext` sætter automatisk `url=...` til adressen på det Whereby-rum som man kommer fra.

Webapp'en skal installeres / kopieres over på jeres egen webserver, – så I ikke er afhængige af demo.solsort.dk på langt sigt. I er dog velkomne til at bruge demo.solsort.dk i starten, til hurtigt at komme i gang.

Ansigtsgenkendelsen kører udelukkende i browseren, og der gemmes ingen data.

### Installation på webserver

Webapp'en er en "statisk webapp", – det vil sige, at det eneste som serveren behøver gøre er, at sende filerne til browseren. Så hvis I allerede har en allerede har en webside hvor I har mulighed for at uploade filer, – så kan I lægge app'en dér.

For at gøre webapp'en tilgængelig på jeres server, skal I blot kopiere/uploade indeholdet af <https://gitlab.com/bibspire/bib-fjernbetjening/-/tree/master/face-detect> over i en mappe på serveren, som webserveren offentligtgøre.  På siden siden som linket refererer til, er der et download-ikon (øverst til højre mellem "Find file" og "Clone"). Når man klikker på download-ikont kan man vælge "Download this directory" – hvor I eksempelvis kan hente en zip-file, der indeholder de ting, der skal ligge på webserveren. Hvordan det uploades afhænger af jeres server.

Da webapp'en benytter kameraet, virker den kun hvis serveren kører `https`. Dette er sandsynligvis allerede er tilfældet. Https betyder, at der er en sikker forbindelse mellem server og browser. Hvis I ikke har https, så bør I opgradere jeres server til at understøtte dette af sikkerhedshensyn. 

Når webapp'en er installeret på jeres egen webserver, skal Chrome-udvidelsen `autoknock-ext` konfigureres til at benytte jeres webadresse i stedet for demo.solsort.dk. Dette er beskrevet tidligere i dette dokument.

### Konfiguration

Når webapp'en ligger på jeres egen server, kan I tilpasse denne ved at ændre i filen `main.js`. Øverst i denne fil er det muligt at tilpasse:

- Hvor tæt ansigtet skal være på kameraet før end den reagerer på det.
- Hvor lang tid den skal se et ansigt før end den sender videre til Whereby.
- Teksten som vises på skærmen, mens den venter på ansigt etc. 
- (Hvilken url den skal redirecte til, hvis `url=...` mangler).

Man ændrer i `main.js` ved at åbne den som et tekstdokument, på samme måde som man også konfigurerer Chrome-extensionen som beskrevet ovenfor. Konfigurationen er også dokumenteret i selve `main.js`.

# Spørgsmål?

Hvis I har spørgsmål eller lignende, er I altid velkomne til at kontakte mig, – se https://solsort.dk
